//
//
// Copyright (C) 2021,  Eric Dorino <eric@dorino.fr>.
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <autosprintf.h>
using gnu::autosprintf;
#include <cstdlib>
#include <cstring>
#include <getopt.h>
#include <iostream>
#include <fstream>
#include <map>
#include <sstream>
#include <stdexcept>
#include <unistd.h>
#include <oxlstream>
#include "split.h"

static void version(), usage();

#define ARG_FONTNAME    256
#define ARG_FONTSIZE    257
#define ARG_FOOTER      258
#define ARG_HEADER      259
#define ARG_VERSION     260

int main(int argc, char **argv)
{
	std::ifstream in,temp2;
	std::ofstream temp1;
	std::vector<std::string> fields;
	int c, delim = '\t';
    char *header = NULL, *footer = NULL, *font_name = NULL;
    unsigned font_size = 10;
#ifdef DEFINE_FORMAT
    std::map<unsigned, const char *> format;
	int column = -1;
	bool verbose = false;
#endif
	bool debug = false;
	char *input = NULL, *output = NULL,
		 tempfile[] = "/tmp/mkexcelXXXXXX";
	static struct option opts[] = {
#ifdef DEFINE_FORMAT
		{"column", required_argument, NULL, 'c'},
#endif
		{"debug", no_argument, NULL, 'D'},
		{"delimiter", required_argument, NULL, 'd'},
		{"font", required_argument, NULL, ARG_FONTNAME},
		{"fontsize", required_argument, NULL, ARG_FONTSIZE},
		{"footer", required_argument, NULL, ARG_FOOTER},
#ifdef DEFINE_FORMAT
		{"format", required_argument, NULL, 'f'},
#endif
		{"header", required_argument, NULL, ARG_HEADER},
		{"help", no_argument, NULL, 'h'},
		{"output", required_argument, NULL, 'o'},
#ifdef DEFINE_FORMAT
		{"verbose", no_argument, NULL, 'V'},
#endif
		{"version", no_argument, NULL, ARG_VERSION},
		{NULL, 0, NULL, 0}
	};
    
    const char *options = 
#ifdef DEFINE_FORMAT
	"c:Dd:hf:o:V";
#else
	"Dd:ho:";
#endif

	unsigned int row, col, max_row, max_col;

	while ((c = getopt_long(argc, argv, options, opts, NULL)) != -1) {
		switch (c) {
#ifdef DEFINE_FORMAT
			case 'c': 
                {
				    std::istringstream s(optarg);
    				s >> column;
    				if (s.bad() || column < 0) {
                        usage();
                        return 1;
                    }
                }
				break;
#endif
			case 'D':
				debug = true;
				break;
			case 'd':
				if (strlen(optarg) == 1)
					delim = optarg[0];
				else if (strlen(optarg) == 3 && 
						(optarg[0] == '\'' || optarg[0] == '"') &&
						optarg[0] == optarg[2])
					delim = optarg[1];
				else { 
					usage();
					return 1;
				}
				break;
			case 'h':
			default:
				usage();
				return 0;
#ifdef DEFINE_FORMAT
			case 'f':
                if (column < 0) {
        			std::cerr << 
                        "mkexcel: format string for no column" << 
                        std::endl;
				    usage();
                    return 1;
                }
                format[column] = optarg;
				break;
#endif
			case 'o':
				if (output) {
					usage();
					return 1;
				}
				output = optarg;
				break;
#ifdef DEFINE_FORMAT
			case 'V':
				verbose = true;
				break;
#endif
			case ARG_FONTNAME:
                font_name = optarg;
                break;
			case ARG_FONTSIZE:
                {
    				std::istringstream s(optarg);
	    			s >> font_size;
    				if (!font_name || s.bad() ||
                        font_size < 6 || font_size > 60) {
                        usage();
                        return 1;
                    }
                }
                break;
			case ARG_FOOTER:
                footer = optarg;
                break;
			case ARG_HEADER:
                header = optarg;
                break;
			case ARG_VERSION:
				version();
				return 0;
		}
	}

	while (optind < argc) {
		if (!input)
			input = argv[optind++];
		else {
			usage();
			return 0;
		}
	}
	
	ed::oxlstream xls;

	if (output)
		xls.open(output);
	else if (input) {
		std::string output(input);
		size_t index;

		index = output.rfind('.');
		if (index == std::string::npos)
			output += ".xls";
		else
			output.replace(index, output.size(), ".xls");
		xls.open(output.c_str());
	}
	else {
		usage();
		return 1;
	}

	if (!xls.good()) {
		std::cerr << "mkexcel: cannot open output file" << std::endl;
		return 1;
	}

	if (input)
		in.open(input);

	if (!debug) {
		if (mkstemp(tempfile) < 0) {
			std::cerr << "mkexcel: cannot create temp file" << std::endl;
			return 1;
		}
	}

	temp1.open(tempfile);
	if (!temp1.good()) {
		std::cerr << "mkexcel: cannot open temp file" << std::endl;
		return 1;
	}

    xls.header(header ? header : (input ? input : "Standard input"));
	xls.footer(footer ? footer : "Page &P");

	xls.page_setup();

    if (font_name) {
        xls.set_default_font(xls.define_font(font_name, font_size));
    }

	max_row = max_col = 0;
	ed::split(input ? in : std::cin, fields, delim);
	while (input ? in.good() : std::cin.good()) {
		max_row++;
		if (max_col < fields.size())
			max_col = fields.size();
		for (size_t i = 0; i < fields.size(); i++) {
			temp1 << fields[i];
			if (i < fields.size()-1)
				temp1.put('\t');
		}
		temp1 << std::endl;
		ed::split(input ? in : std::cin, fields, delim);
	}

	temp1.close();

	temp2.open(tempfile);
	if (!temp2.good()) {
		std::cerr << "mkexcel: cannot open temp file" << std::endl;
		return 1;
	}

	ed::split(temp2, fields, '\t');
	row = 0;
	col = 1;
	while (temp2.good()) {
		row++;
		col = 1;
		for (size_t i = 0; i < fields.size(); i++) {
			try {
				std::istringstream s(fields[i]);
				long long_val;
				double double_val;

				s >> long_val;
				if (s.eof() && !s.bad())
					xls << long_val;
				else {
					s.str(fields[i]);
					s >> double_val;
					if (s.eof() && !s.bad())
						xls << double_val;
					else
						xls << fields[i];
				}
				col++;
			} catch (std::length_error &e) {
				std::cerr << 
					autosprintf("%s: (%d, %d): field truncated",
							input ? input : "<stdin>", row, col) <<
					std::endl;	
				xls << fields[i].substr(0, 255);
				col++;
			}
		}
		xls << std::endl;
		ed::split(temp2, fields, '\t');
	}

	temp2.close();
	xls.close();
	if (input)
		in.close();
	if (!debug)
		unlink(tempfile);

	return 0;
}

void version() {
		std::cerr <<
			autosprintf("%s %s\n\nCopyright (C) 2021, Eric Dorino\n"
					"This is free software; see the source for copying "
					"conditions.  There is NO\nwarranty; not even for "
					"MERCHANTABILITY of FITNESS FOR A PARTICULAR "
					"PURPOSE.", PACKAGE, VERSION) <<
			std::endl;
}
void usage()
{
	std::cerr <<
		"usage: mkexcel [options][inputfile]" <<
		std::endl <<
		"usage: mkexcel --help" <<
		std::endl;
}
