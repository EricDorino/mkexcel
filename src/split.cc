/*
 * Copyright (C) 2021, Eric Dorino <eric@dorino.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "split.h"

static std::istream &field(std::istream &, int &, std::string &, char);
static std::istream &crlf(std::istream &, int &);

std::istream &
ed::split(std::istream &stream, std::vector<std::string> &result, char delim)
{
    int c;
    std::string str;

    result.clear();

    c = stream.get();
    if (!stream.good())
        return stream;
    field(stream, c, str, delim);
    result.push_back(str);
    while (stream.good() && c == delim) {
        c = stream.get();
        field(stream, c, str, delim);
        result.push_back(str);
    }
    crlf(stream, c);

    return stream;
}

std::istream &
field(std::istream &stream, int &c, std::string &str, char delim)
{
    str.clear();
    if (c == '"') {
        c = stream.get();
        for (;;) {
            while (stream.good() && c != '"') {
                str.push_back(c);
                c = stream.get();
            }
            c = stream.get();
            if (c == '"') {
                str.push_back('"');
                c = stream.get();
                continue;
            }
            else break;
        }
    }
    else {
        while (stream.good() && c != delim && c != '\r' && c != '\n') {
            str.push_back(c);
            c = stream.get();
        }
    }
    return stream;
}

std::istream &crlf(std::istream &stream, int &c)
{

    if (stream.good()) {
        if (c == '\r') {
            c = stream.get();
            if (c != '\n')
                stream.unget();
        }
        else if (c != '\n')
            stream.unget();
    }
    return stream;
}
